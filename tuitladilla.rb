require 'twitter'
require 'dotenv'
require 'date'

client = Twitter::REST::Client.new do |config|
  config.consumer_key        = ENV["CONSUMER_KEY"]
  config.consumer_secret     = ENV["CONSUMER_SECRET"]
  config.access_token        = ENV["ACCESS_TOKEN"]
  config.access_token_secret = ENV["ACCESS_SECRET"]
end

hour = DateTime.now.strftime('%H:%M')
date = DateTime.now.strftime('%d/%m/%Y')

message = "Son las #{hour} del #{date} y aún no han resuelto mi incidencia número #{ENV['INCIDENT_NUMBER']}. @movistar_es ¿Será que van a responder? Gracias"

client.update(message)
